'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var postcss = require('gulp-postcss');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify-es').default;
var jsImport = require('gulp-js-import-file');
var comments = require('postcss-discard-comments');
var merge = require('merge-stream');
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  var home = gulp.src('./styles/theme.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ comments({removeAll: true}), autoprefixer(), cssnano() ]))
    //.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../assets/'));

var collection = gulp.src('./styles/collection.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ comments({removeAll: true}), autoprefixer(), cssnano() ]))
    //.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../assets/'));

var product = gulp.src('./styles/product.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ comments({removeAll: true}), autoprefixer(), cssnano() ]))
    //.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('../assets/'));

return merge(home, collection, product)
});

gulp.task('js', function() {
    var theme = gulp.src('./js/theme.js')
        .pipe(jsImport({
            hideConsole: true,
            importStack: true,
            es6import: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/'));

    var collection = gulp.src('./js/template-collection.js')
        .pipe(jsImport({
            hideConsole: true,
            importStack: true,
            es6import: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/'));

    var product = gulp.src('./js/template-product.js')
        .pipe(jsImport({
            hideConsole: true,
            importStack: true,
            es6import: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/'));
    
    var search = gulp.src('./js/template-search.js')
        .pipe(jsImport({
            hideConsole: true,
            importStack: true,
            es6import: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest('../assets/'));
    
    return merge(theme, collection, product, search)
});
 
gulp.task('watchify', function () {
  gulp.watch('./styles/**/*.scss', gulp.series('sass'));
  gulp.watch('./js/**/*.js', gulp.series('js'));
});

gulp.task('watch', gulp.series('sass', 'js', 'watchify'));