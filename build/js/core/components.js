$(function() {
    //featured image toggle
    let user = detect.parse(navigator.userAgent);
    $('body').addClass(user.browser.family + user.browser.version);
  
    $('.nav_holder nav > ul > li ul').on("click", function(e) {
      e.stopPropagation();
    });
  
    $('body').on('click', function() {
      $('body').removeClass('active-nav');
      $('.nav_holder nav > ul > li').removeClass('active-nav');
    });
    //Allow for tooltips
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl, {
        container: 'body'
      })
    })
  
    //toggle scroll on header
    $(window).scroll(function() {
      var height = $(window).scrollTop();
      if(height  > 60) {
        $('#shopify-section-header, .top-nav').addClass('scrolled');
        $('.white-logo').hide();
        $('.alt-logo').show();
      } else {
        $('#shopify-section-header, .top-nav').removeClass('scrolled');
        $('.white-logo').show();
        $('.alt-logo').hide();
      }
    });
  
    $("#show-filters button").on( "click", function () {
      $('.collection-list-holder, .collection-sidebar').toggleClass('active-filters')
    });
  
    $("[data-href]").on( "click", function () {
      let href = $(this).data('href');
      window.location.href = href;
    });
    $("[data-link-type]").on("click", function(e) {
      e.preventDefault();
    });
    $('a[href="#Login"]').on('click', function() {
      $('#Login').toggleClass('active');
      return false;
    });
  
    $("[data-media]").on( "click", function () {
      let media = $(this).data('media');
      $('.media-modal').show();
      $('.media-modal .media').html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+media+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
    });
  
    $(document).on("click", ".media-modal_close, .where-to-buy-modal_close", function() {
      $('.media-modal').hide();
      $('.media-modal .media').html('');
      $('.where-to-buy-modal').hide();
    });
  
    $('#video-modal .close').on('click', function() {
      var player = $('#video-modal iframe');
      var vidSrc = player.prop('src');
      player.prop('src', ''); // to force it to pause
      player.prop('src', vidSrc);
    })
    $('#assembly-video-modal .close').on('click', function() {
      var player = $('#assembly-video-modal iframe');
      var vidSrc = player.prop('src');
      player.prop('src', ''); // to force it to pause
      player.prop('src', vidSrc);
    })
  
    $('[data-wtb]').on('click', function() {
      $('.where-to-buy-modal').show();
      return false;
    });
  
    $('#create_customer').submit(function(e) {
      if ( $('input[name="customer[password]"]').val() != $('input[name="customer[password_confirmation]"]').val()) {
        e.preventDefault();
        alert('Passwords do not match.');
      }
    });
  
  });
  $(window).on("load", function() {
    $('#site-preloader').fadeOut();
    $('#main-content').fadeTo(400, 1);
  });
  