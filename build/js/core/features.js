$(function() {
    parallax();
    inView( '.animate' ).on( 'enter', function(el) {
        $( el ).addClass( 'visible' );
    } );

    $(window).scroll(function () {
        parallax();
    });

    $(".instant-buy-button").on('click', function(e) {
        var $ = jQuery;
        var formParams = $(this).closest('form').serialize();
        var $this = $(this);
        $(this).html('<i class="fas fa-spinner"></i> Opening Crate').addClass('adding-to-cart').attr('disabled');
        $.ajax({
            url: "/cart/add",
            type: "post",
            data: formParams,
            success: function(){
                window.location.href = "/checkout";
            },
            error: function(){
            }
        })
        return false;
    });
});

function parallax() {
    var ww = $( window ).width();
    $('.parallax').each(function() {
        if(($(this).hasClass('parallax-no-mobile') && ww > 991) || !$(this).hasClass('parallax-no-mobile')) {
            if($(this).hasClass('page-hero')) {
                var x = $(this).offset().top - $(window).scrollTop()-120;
            } else {
                var x = $(this).offset().top - $(window).scrollTop();
            }
            if (x > 0) {
                x = '-' + x;
            } else {
                x = Math.abs(x);
            }
            let slider_height = $(this).outerHeight();
            let slider_pos = parseInt(x / 2);
            if($(this).hasClass('parallax-slider')) {
                $(this).find(".slick-slide").each(function() {
                    $(this).children('.slider-image').css('background-position', 'center top ' + parseInt(x / 2) + 'px');
                });
            } else if($(this).hasClass('parallax-content')) {
                $(this).css('transform', 'translateY('+parseInt(x / 7)+'px)')
            } else {
                $(this).css('background-position', 'center top ' + slider_pos + 'px');
            }
        }
    })
}

