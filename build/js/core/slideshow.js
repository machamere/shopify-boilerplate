//Zoom Properties
// $('.product-image--slider').on('init', zoomTrigger);
// $('.product-image--slider').on('afterChange', zoomTrigger);


//Functions
function zoomTrigger() {
    $('.slick-current [data-zoom-image]').elevateZoom({
        tint:true,
        tintColour:'#000',
        tintOpacity:0.5
    });
}