$(function() {
    let mobile_menu = $('.mobile-menu');
    let mobile_menu_li = $('.mobile-menu div nav > ul > li');
    let mobile_menu_arrow = $('.mobile-menu .fas');

    $(mobile_menu_arrow).on('click', function(e) {
        e.preventDefault();
        if($(this).closest('li').hasClass('active')) {
            $(mobile_menu_li).removeClass('active inactive');
        } else {
            $(mobile_menu_li).removeClass('active').addClass('inactive');
            $(this).closest('li').addClass('active').removeClass('inactive');
        }
    })
    
    //Announcement
    let cookieValue = $.cookie("close-holiday-announcement");
    if(!cookieValue) {
        $(".announcement-bar").show();
        let ann_bar_height = $(".announcement-bar").height();
        $(".mobile-menu").css('padding-top', ann_bar_height+100+'px');
    }
    $('#close-announcement').on('click',function(){
        $.cookie("close-holiday-announcement", 1);
        $(".announcement-bar").slideToggle(400);
        $(".mobile-menu").css('padding-top', 100+'px');
    });

    //header toggles
    $(".nav_toggle").on('click', function() {
        $('.header').toggleClass('active');
        $('.mobile-menu').toggleClass('active');
    });

    $("#account-icon").on('click', function () {
        $("#header-account-nav").toggleClass('active');
        $('#MainContent').toggleClass('account-nav-active');
        $('.header, .search_holder').removeClass('search-active');
    });

    $(".search, .search-close").on('click', function() {
        $('.header').toggleClass('search-active');
        $('.search_holder').toggleClass('search-active');
        $("#header-account-nav").removeClass('active');
        $('#MainContent').removeClass('account-nav-active');
    });

    $('.nav_holder nav > ul > li').on({
        "mouseover": function (e) {
            let $this = $(this);
            $('.nav_holder nav > ul > li').removeClass('active-nav');
            $('body').addClass('active-nav');
            $($this).addClass('active-nav');
        },
        "mouseleave": function (e) {
            let $this = $(this);
            $('.nav_holder nav > ul > li').removeClass('active-nav');
            $('body').removeClass('active-nav');
            $($this).removeClass('active-nav');
        }
    });
})