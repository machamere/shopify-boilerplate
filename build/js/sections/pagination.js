$(function() {
  var $pagination = $('.pagination');
  $pagination.on('click', function() {
    let page = $pagination.pagination('getCurrentPage');
    var colors = [];
    var tags = [];
    if($('#filter-by-color .filter.active').length > 0) {
      $('#filter-by-color .filter.active').each(function () {
        colors.push($(this).find('a').attr('data-color'));
      })
    }
    if($('#tag-filter .filter.active').length > 0) {
      $('#tag-filter .filter.active').each(function () {
        tags.push($(this).find('a').attr('data-tags'));
      })
    }
    //console.log(colors)
    loadCollectionProducts(tags, colors, page, 'pagination');
  })
})