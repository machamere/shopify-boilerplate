$(function() {
  let filterColor = false;
  let filterTag = false;
  if($("#color-filter").length != 0) {
    filterColor = true;
  }
  if($("#tag-filter").length != 0) {
    filterTag = true;
  }
  getFilters(filterTag, filterColor);
  var tags = [];
  var colors = [];
  $('.filter-contain').on("click", ".filter a", function(e){
    e.preventDefault();
    $('.pagination').pagination('selectPage', 1);
    $(this).parent().toggleClass('active');
    if($(this).attr('data-tags')) {
      var idx = $.inArray($(this).attr('data-tags'), tags);
      if (idx == -1) {
        tags.push($(this).attr('data-tags'));
      } else {
        tags.splice(idx, 1);
      }
    }
    if($(this).attr('data-color')) {
      var idx = $.inArray($(this).attr('data-color'), colors);
      if (idx == -1) {
        colors.push($(this).attr('data-color'));
      } else {
        colors.splice(idx, 1);
      }
    }
    loadCollectionProducts(tags, colors);
  })
});

function getFilters(filterTags = false, filterColors = false) {
  const collection = window.location.pathname;
  $.ajax({
    url: collection+"/products.json",
    type: 'GET',
    dataType: 'json'
  })
  .done(function (data) {
    console.log(data)
    var colors = [];
    var tags = [];
    var currentTags = '';
    $('#filter-by-color ul, #filter-by-tags ul').html('');
    //loop products
    $.each(data.products, function(k, v) {
      var urlParams = new URLSearchParams(window.location.search); 
      if(urlParams.get('tag')) {
        currentTags = ','+urlParams.get('tag').replace(/\ /g, "+");
      }
      if(filterTags) {
        $.each(v.tags, function(i, t) {
          if ($.inArrayIn(t, tags) == -1) tags.push(t);
        })
      }
      //colors
      if(filterColors) {
        $.each(v.options, function(i, o) {
          //console.log(o)
          if(o.name == 'Color') {
            $.each(o.values, function(i, c) {
              if ($.inArrayIn(c, colors) == -1) colors.push(c);
            })
          }
        });
      }
    });
    if(filterColors) {
      $.each(colors, function(k, v) {
        $('#filter-by-color ul').append('<li class="filter"><a href="?color='+v.toLowerCase().replace(/\ /g, "+")+'" data-color="'+v.toLowerCase()+'">'+v+'</a></li>')
      });
    }
    if(filterTags) {
      $.each(tags, function(k, v) {
        let newTag = '';
        let activeClass = '';
        let stripTags = currentTags;
        if(!currentTags.includes(v.toLowerCase().replace(/\ /g, "+"))) {
          newTag = v.toLowerCase().replace(/\ /g, "+");
        } else {
          let needle = v.toLowerCase().replace(/\ /g, "+");
          stripTags = currentTags.replace(needle, '').split(',').filter(item => item).toString();
          activeClass = ' active'
        }
        $('#filter-by-tags ul').append('<li class="filter'+activeClass+'"><a href="?tag='+newTag+stripTags+'" data-tags="'+v.toLowerCase()+'">'+v+'</a></li>')
      });
    }
  })
}