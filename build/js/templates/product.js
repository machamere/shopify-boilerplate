$(function() {
    //start slick slider
    $('#product-image--slider #main-product-image').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#main-product-thumbs'
    });
    $('#main-product-thumbs').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '#main-product-image',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        prevArrow: '<button class="carousel-control-prev" type="button"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button>',
        nextArrow: '<button class="carousel-control-next" type="button"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next</span></button>'
    });

    const curSym = $('.price-item--regular').text().trim().charAt(0);
    let type = '';
    val = '';
    if($('.simple-variant').length) {
        type = 'simple';
    } else if($('.advanced-variant').length) {
        type = 'advanced';
        //value sync
        val = $("#ProductSelect-product-template").find(':selected').data('title');
    }
    variantSwitch(val, curSym, type);
    productImages(val);
    $('.single-option-selector').on("change", function() {
        if($('.simple-variant').length) {
            val = $(this).val();
        } else if($('.advanced-variant').length) {
            val = $('input[name="color"]:checked').val();
        }
        $("#ProductSelect-product-template option[data-title='" + val + "']").prop("selected", true);
        variantSwitch(val, curSym, type);
        productImages();
    })
});

function productImages(currentSelected = '') {
    $.ajax({
        url: window.location.href+".js",
        type: 'GET',
        dataType: 'json'
    })
    .done(function (data) {
        //console.log(data)
        let currentFI = '';
        const variants = data.variants;
        if($('.simple-variant').length && currentSelected == '') {
            currentSelected = $('.single-option-selector').val();
        } else if($('.advanced-variant').length && currentSelected == '') {
            currentSelected = $('input[name="color"]:checked').val();
        }
        
        let featuredImages = []
        $(variants).each(function (i,v) {
            featuredImages.push(v.featured_image.position);
            if(currentSelected == v.title) {
                currentFI = v.featured_image.position;
            }
        });
        const sortFI = featuredImages.sort(function(a, b){return a-b});
        const fiCurrentPos = sortFI.indexOf(currentFI);
        const fiNextPos = fiCurrentPos+1;
        const $prodImageSlider = $('#product-image--slider #main-product-image');
        const $prodThumbs = $('#product-thumbs #main-product-thumbs')
        $prodThumbs.slick('slickRemove', null, null, true);
        $prodImageSlider.slick('slickRemove', null, null, true);
        $(data.media).each(function (a,v) {
            //imageSizes
            let fileExtension = v.src.replace(/^.*\./, '');
            let img1x = v.src.replace('.'+fileExtension, '_1x.'+fileExtension);
            let thumbSm = v.src.replace('.'+fileExtension, '_100x.'+fileExtension);
            let thumbMd = v.src.replace('.'+fileExtension, '_180x.'+fileExtension);
            let thumbLg = v.src.replace('.'+fileExtension, '_360x.'+fileExtension);
            let slideSm = v.src.replace('.'+fileExtension, '_400x.'+fileExtension);
            let slideMd = v.src.replace('.'+fileExtension, '_700x.'+fileExtension);
            let slideLg = v.src.replace('.'+fileExtension, '_1000x.'+fileExtension);
            if(sortFI[fiNextPos] && v.position >= sortFI[fiCurrentPos] && v.position < sortFI[fiNextPos]) {
                $prodImageSlider.slick('slickAdd', '<div class="slick-slide"><img class="lazyload" src="'+img1x+'" data-srcset="'+slideSm+' 400w,'+slideMd+' 700w,'+slideLg+' 1000w" data-sizes="auto" alt="'+v.alt+'" /></div>');
                $prodThumbs.slick('slickAdd', '<div><img class="lazyload" src="'+img1x+'" data-srcset="'+thumbSm+' 100w,'+thumbMd+' 180w,'+thumbLg+' 360w" data-sizes="auto" alt="'+v.alt+'" /></div>');
            } else if(!sortFI[fiNextPos] && v.position >= sortFI[fiCurrentPos]) {
                $prodImageSlider.slick('slickAdd', '<div class="slick-slide"><img class="lazyload" src="'+img1x+'" data-srcset="'+slideSm+' 400w,'+slideMd+' 700w,'+slideLg+' 1000w" data-sizes="auto" alt="'+v.alt+'" /></div>');
                $prodThumbs.slick('slickAdd', '<div><img class="lazyload" src="'+img1x+'" data-srcset="'+thumbSm+' 100w,'+thumbMd+' 180w,'+thumbLg+' 360w" data-sizes="auto" alt="'+v.alt+'" /></div>');
            }
        });
    })
}

function variantSwitch(checker, curSym, type) {
    var available = false
    $.ajax({
        url: window.location.href+".js",
        type: 'GET',
        dataType: 'json'
    })
    .done(function (data) {
        //console.log(data);
        const variants = data.variants;
        const addToCartText = $('[data-add-to-cart]').attr('aria-label');
        $(variants).each(function (i,v) {
            if(checker == v.title) {
                //console.log(v);
                let $priceHtml = '';
                const price = curSym+(v.price/100).toFixed(2);
                const comparePrice = curSym+(v.compare_at_price/100).toFixed(2);
                const model = v.public_title;
                let onSale = false;
                let priceClass = 'price';
                
                if(v.compare_at_price > v.price) {
                    onSale = true;
                    priceClass += ' price--on-sale';
                }
                //Replace Model
                if(type == 'advanced') {
                    $('#model-color').text(model);
                }

                //Start $priceHtml
                $priceHtml += '<dl class="'+priceClass+'">';

                if(onSale) {
                    $priceHtml +=
                        '<div class="price__sale">'+
                            '<dt>'+
                                '<span class="visually-hidden visually-hidden--inline">'+comparePrice+'</span>'+
                            '</dt>'+
                            '<dd>'+
                                '<span class="price-item price-item--sale" data-sale-price>'+
                                    price+
                                '</span>'+
                            '</dd>'+
                            '<dt>'+
                                '<span class="visually-hidden visually-hidden--inline">'+price+'</span>'+
                            '</dt>'+
                            '<dd>'+
                                '<s class="price-item price-item--regular" data-regular-price>'+
                                    comparePrice+
                                '</s>'+
                            '</dd>'+
                        '</div>';
                } else {
                    //Set Normal Price
                    $priceHtml +=
                    '<div class="price__regular">'+
                        '<dt>'+
                            '<span class="visually-hidden visually-hidden--inline">Regular price</span>'+
                        '</dt>'+
                        '<dd>'+
                            '<span span class="price-item price-item--regular" data-regular-price="">'+
                                price+
                            '</span>'+
                        '</dd>'+
                    '</div>';
                }

                //End $priceHtml
                $priceHtml += '</dl>';
                $('.product-price').html($priceHtml);
                if(v.available) {
                    $('[data-add-to-cart] [data-add-to-cart-text]').text(addToCartText).closest('button').prop('disabled', false);
                    $('.product-out-of-stock').hide();
                } else {
                    $('[data-add-to-cart] [data-add-to-cart-text]').text('Out of Stock').closest('button').prop('disabled', true);
                    $('.product-out-of-stock').show();
                }
                return false
            }
        });
    })
}