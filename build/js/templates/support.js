$(document).ready(function(){
    $("#category").change(function() {
        $('#prettydropdown-product').slideUp(400);
        $('#support-info .tearsheet, #support-info .assembly').slideUp(400);
        let value = $( "#category option:selected" ).val();
        if(value != 'Select Category') {
            $('[data-value="Select Category"]').slideUp(400);
        }
        $.ajax({
            url: "/pages/product-ajax",
            type: 'GET'
        })
        .done(function (data) {
            let product = $.parseJSON(data);
            let value = $( "#category option:selected" ).val();
            $('#product').find('option').remove().end();
            $('#product').append('<option>Select Product</option>');
            $(product).each(function (index) {
                //console.log(this.type);
                if(value === this.type) {
                    //console.log('added');
                    $('#product').append('<option value="' + this.id + '">' + this.title + '</option>');
                }
            });
            $('.support-product').slideDown(400);
            $('#product').prettyDropdown({width: '100%'}).refresh();
        })
        .fail(function (data) {
        })
        .always(function (data) {
        });
    });

    $("#product").change(function() {
        let value = $( "#product option:selected" ).val();
        if(value != 'Select Product') {
            $('[data-value="Select Product"]').slideUp(400);
        }
        $.ajax({
            url: "/pages/product-ajax",
            type: 'GET'
        })
            .done(function (data) {
                let product = $.parseJSON(data);
                let value = $( "#product option:selected" ).val();
                $(product).each(function (index) {
                    //console.log(this.id);
                    if(value == this.id) {
                        //console.log('added');
                        if(this.tearsheet != '') {
                            $('#support-info .tearsheet').slideDown(400);
                            $('#tearsheet-link').attr('href', this.tearsheet);
                        } else {
                            $('#support-info .tearsheet').slideUp(400);
                        }
                        if(this.assembly != '') {
                            $('#support-info .assembly').slideDown(400);
                            $('#assembly-link').attr('href', this.assembly);
                        } else {
                            $('#support-info .assembly').slideUp(400);
                        }
                    }
                });

            })
            .fail(function (data) {
            })
            .always(function (data) {
            });
    });

    $('.warranty-form #contact_form').submit(function(e) {        
        console.log('submit valid');
        $('button[type=submit]').attr('disabled', true).addClass('button_disabled btn_disabled').html('<i class="fas fa-spinner"></i> Submitting');
    });

    if(getUrlParameter('contact_posted') === 'true') {
        $('.form-message--success').show();
    }
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

/*
$.ajax({
    url: "/pages/product-ajax",
    type: 'GET'
})
.done(function (data) {
    var product = $.parseJSON(data);
    $(product).each(function (index) {
        console.log(this.id);
    });
})
.fail(function (data) {
})
.always(function (data) {
});$.ajax({
    url: "/pages/product-ajax",
    type: 'GET'
})
.done(function (data) {
    var product = $.parseJSON(data);
    $(product).each(function (index) {
        console.log(this.id);
    });
})
.fail(function (data) {
})
.always(function (data) {
});
 */