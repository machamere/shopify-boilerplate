$(function() {
  loadCollectionProducts();

  const colorHolder = $('.card-row');
  if($('.collection-list').hasClass('active-hover')) {
    colorHolder.on("mouseenter click", ".color-holder label", function(e) {
      e.stopImmediatePropagation();
      //console.log(prodURL)
      if(!$(this).hasClass('active')) {
        updateCardThumb($(this))
      }
      return false;
    });
  } else {
    colorHolder.on("click", ".color-holder label", function(e) {
      e.stopImmediatePropagation();
      //console.log(prodURL)
      if(!$(this).hasClass('active')) {
        updateCardThumb($(this))
      }
      return false;
    });
  }

  $('[data-production-href]').on('click', function() {
    let href = $(this).data('production-href');
    window.location.href = href;
    return false;
  })
})

function loadCollectionProducts(tags = '', colors = '', page = 1, location = false) {
  var pathname = window.location.pathname,
      recPerPage = $('.col-collection-cards').data('limit'),
      $pagination = $('.pagination'),
      totalRecords = $('#product-infos').data('total-products');
      filterRecords = 0;

  if(tags == '' && colors == '') {
    $pagination.pagination({
      items: totalRecords,
      itemsOnPage: recPerPage,
      currentPage: page
    })
  }

  if(tags != '' || colors != '') {
    var recPerPageFilter = recPerPage;
    var startingPoint = (page-1)*recPerPage;
    var endingPoint = page*recPerPage;
    recPerPage = '';
    page = '';
  }

  $.ajax({
    url: pathname+"/products.json?limit="+recPerPage+"&page="+page,
    type: 'GET',
    dataType: 'json',
    beforeSend: function() {
      $('.col-collection-cards .card-row-placehold').removeClass('d-none');
      $('.col-collection-cards .card-row').html('');
    }
  })
  .done(function (data) {
    $('.col-collection-cards .card-row-placehold').addClass('d-none');
    $.each(data.products, function(k, v) {
      let prices = [];
      let price_varies = false;
      let product_tags = v.tags.map(t => t.toLowerCase());
      let prod_color = [];
      $.each(v.variants, function(i, p) {
        if ($.inArrayIn(p.price, prices) == -1) prices.push(p.price);
        if ($.inArrayIn(p.title, prod_color) == -1) prod_color.push(p.title);
      });//ends each variant
      prod_color = prod_color.map(t => t.toLowerCase());
      // console.log('Selected Colors: '+colors);
      // console.log('available colors: '+prod_color)
      prices.sort()
      if(prices.length > 1) {
        price_varies = true;
      }
      if(tags != '' && colors != '' && !location) {
        //console.log(colors)
        if(tags.some(ai => product_tags.includes(ai)) && colors.some(pc => prod_color.includes(pc))) {
          filterRecords++;
          if(filterRecords <= recPerPageFilter) {
            createCard(v.id, v.handle, v.title, v.vendor, prices[0], price_varies, v.variants[0].featured_image.src, v.variants);
          }
        }
      } else if((tags != '' || colors != '') && !location) {
        //console.log('Tags filter')
        if(tags.some(ai => product_tags.includes(ai)) || colors.some(pc => prod_color.includes(pc))) {
          filterRecords++;
          if(filterRecords <= recPerPageFilter) {
            createCard(v.id, v.handle, v.title, v.vendor, prices[0], price_varies, v.variants[0].featured_image.src, v.variants);
          }
        }
      } else if(tags != '' && colors != '' && location == 'pagination') {
        //console.log(colors)
        if(tags.some(ai => product_tags.includes(ai)) && colors.some(pc => prod_color.includes(pc))) {
          filterRecords++;
          if(startingPoint < filterRecords && filterRecords <= endingPoint) {
            createCard(v.id, v.handle, v.title, v.vendor, prices[0], price_varies, v.variants[0].featured_image.src, v.variants);
          }
        }
      } else if((tags != '' || colors != '') && location == 'pagination') {
        //console.log(data.products.length)
        if(tags.some(ai => product_tags.includes(ai)) || colors.some(pc => prod_color.includes(pc))) {
          filterRecords++;
          // console.log('Starting vs Records: '+startingPoint+' : '+filterRecords)
          // console.log('Records vs Ending: '+filterRecords+' : '+endingPoint)
          if(startingPoint < filterRecords && filterRecords <= endingPoint) {
            createCard(v.id, v.handle, v.title, v.vendor, prices[0], price_varies, v.variants[0].featured_image.src, v.variants);
          }
        }
      } else {
        createCard(v.id, v.handle, v.title, v.vendor, prices[0], price_varies, v.variants[0].featured_image.src, v.variants);
      }
    });//ends each product
    //console.log(recPerPage)
    if(tags != '' || colors != '') {
      $pagination.pagination('updateItems', filterRecords);
    }
  });
}


function createCard(id, url, title, vendor, price, price_varies = false, image, variants) {
  let priceHTML = price;
  let colorsHtml = '';
  let fileExtension = image.replace(/^.*\./, '');
  let smallImg = image.replace('.'+fileExtension, '_1x.'+fileExtension);
  let imgSrcSm = image.replace('.'+fileExtension, '_300x.'+fileExtension);
  let imgSrcMd = image.replace('.'+fileExtension, '_600x.'+fileExtension);
  let imgSrcLg = image.replace('.'+fileExtension, '_800x.'+fileExtension);
  if(price_varies) {
    priceHTML = '<small class="text-muted">Starting at</small> '+
      price+
      '<span aria-hidden="true">+</span>';
  }

  $.each(variants, function(i, v) {
    let color = v.title.toLowerCase();
    let customSwatch = $('[data-prod-id*="'+v.product_id+'"]').data('swatch-img');
    let style = 'background-color: '+color;
    if(customSwatch) {
      let swatch = customSwatch.split(',');
      style = 'background-image: url('+swatch[i]+');';
    }
    colorsHtml += '<div class="color-holder col-auto">'+    
      '<input type="radio" id=Option-'+v.title.replace(" ", "-").toLowerCase()+'-position-'+id+'" name="options[Color]-position-'+id+'" value="'+v.title+'"';
    if(i === 0) {
      colorsHtml += ' checked'
    }
    colorsHtml += '>'+ 
      '<label class="';
    if(!v.available) {
      colorsHtml += ' out-of-stock';
    }
    if(i === 0) {
      colorsHtml += ' active'
    }
    colorsHtml += '" style="'+style+'" for="Option-'+v.title.replace(" ", "-").toLowerCase()+'-position-'+id+'" data-bs-toggle="tooltip" data-bs-placement="top" title="'+v.title+'">'+v.title+'</label>'+ 
      '</div>';
  });
          
  $('.col-collection-cards .card-row').append(
    '<div class="col">'+
      '<div class="card bg-light h-100" data-production-href="/products/'+url+'">'+
        //{% if product.compare_at_price_max > product.price %}
          //<div class="sale badge bg-dark text-info"><i class="bi bi-tag-fill text-danger text-large"></i>{% if section.settings.show_percent_off %} <span class="percent-off"><small>up to</small> {{ product.compare_at_price_max | minus: product.price | times: 100.0 | divided_by: product.compare_at_price_max }}%</span>{% endif %}</div>
        //{% endif %}
        '<img src="'+smallImg+'" data-srcset="'+imgSrcSm+' 300w,'+imgSrcMd+' 600w,'+imgSrcLg+' 800w" data-sizes="auto" alt="" class="card-img-top lazyload" alt="'+title+'">'+
        '<div class="spinner-holder">'+
          '<div class="d-flex justify-content-center">'+
            '<div class="spinner-border text-primary" role="status">'+
              '<span class="visually-hidden">Loading...</span>'+
            '</div>'+
          '</div>'+
        '</div>'+
        '<div class="card-body">'+
          '<h5 class="card-title">'+title+'</h5>'+
          //{% if section.settings.show_price %}
          '<p class="card-text">'+
            priceHTML+
          '</p>'+
          // {% endif %}
          // {% if section.settings.show_colors %}
            //{%- assign featuredID = product.featured_image.id -%}
            '<div class="colors row g-1">'+
              // {% for variant in product.variants %}
              //     {% assign count = count | plus: 1 %}
              //     {% assign has_image = 'true' %}
              //     {% assign color = variant.metafields.variant.color %}
              //     {% assign swatchImage = variant.metafields.variant.swatch %}
              //     {% assign has_image = false %}
              //     {%- capture used_colors -%}
              //         {{ used_colors }}::{{ variant.title }}

              //     {%- endcapture -%}
              //     {%- liquid
              //         if color == blank and swatchImage == blank
              //           assign color = variant.title | downcase
              //         endif
              //         if swatchImage != blank
              //           assign has_image = true
              //         endif
              //     -%}
              //     {% unless has_image %}
              //       {%- capture style -%}
              //         background-color: {{ color }};
              //       {%- endcapture -%}
              //     {% else %}
              //       {% assign swatchImage = swatchImage | split: '/' | last | split: '?' | first %}
              //       {%- capture style -%}
              //         background-image: url({{ swatchImage | file_img_url: '30x' }});
              //       {%- endcapture -%}
              //     {% endunless %}
              //     <div class="color-holder col-auto">        
              //       <input type="radio" id="Option-{{ variant.title | replace: " ", "-" }}-position-{{ product.id }}" name="options[Color]-position-{{ product.id }}" value="{{ variant.title }}"{% if count == 1 %} checked{% endif %}>
              //       <label class="{% unless variant.inventory_quantity > 0 %} out-of-stock{% endunless %}{% if variant.featured_image.id == featuredID %} active{% endif %}" style="{{ style }}" for="Option-{{ variant.title | replace: " ", "-"  }}-position-{{ product.id }}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ variant.title }}">{% unless variant.inventory_quantity > 0 %}<s>{% endunless %}{{variant.title}}{% unless variant.inventory_quantity > 0 %}</s>{% endunless %}</label>
              //     </div>
              // {% endfor %}
              colorsHtml+
            '</div>'+
          // {% endif %}
          // {% if section.settings.show_vendor %}
            '<p class="card-text">'+
              '<small class="text-muted">by '+vendor+'</small>'+
            '</p>'+
          //{% endif %}
        '</div>'+
        '<div class="card-footer">'+
          '<a href="/products/'+url+'" class="btn btn-primary d-grid">View</a>'+
        '</div>'+
      '</div>'+
    '</div>'
    )
}

function updateCardThumb($this) {
  const prodURL = $this.closest('.card').find('.card-footer a').attr('href');
  $this.closest('.card').find('.card-img-top').remove();
  $this.closest('.card').find('.color-holder label').removeClass('active');
  $this.addClass('active');
  $.ajax({
    url: prodURL+".js",
    type: 'GET',
    dataType: 'json'
  })
  .done(function (data) {
      const variants = data.variants;
      const val = $this.text();
      $(variants).each(function (i,v) {
        if(val == v.title) {
          //console.log(v)
          let fileExtension = v.featured_image.src.replace(/^.*\./, '');
          let smallImg = v.featured_image.src.replace('.'+fileExtension, '_1x.'+fileExtension);
          let imgSrcSm = v.featured_image.src.replace('.'+fileExtension, '_300x.'+fileExtension);
          let imgSrcMd = v.featured_image.src.replace('.'+fileExtension, '_600x.'+fileExtension);
          let imgSrcLg = v.featured_image.src.replace('.'+fileExtension, '_800x.'+fileExtension);
          $this.closest('.card').prepend('<img src="'+smallImg+'" data-srcset="'+imgSrcSm+' 300w,'+imgSrcMd+' 600w,'+imgSrcLg+' 800w" data-sizes="auto" alt="" class="card-img-top lazyload" alt="'+v.name+'">');
          //$this.closest('.card').find('.spinner-holder').remove();
        }
      });
  })
}