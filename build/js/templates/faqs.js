$(function() {
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    $("input[name='faq-search']").keyup(function() {
        keyword_search($(this).val());
    });

    var acc = document.getElementsByClassName("question");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            this.parentElement.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
});

function keyword_search(keyword) {
    console.log(keyword);
    $(".faq-section, .popular-faq, .all-faq .faq, .sections-faq").css('display', '');
    if(keyword.length > 0) {
        $('.popular-faq, .sections-faq, .all-faq .faq').css('display', 'none');
        $(".all-faq .faq:contains(" + keyword + ")").css('display', '');
        $(".all-faq .faq-section").each(function () {
            var wrap = $(this);
            if (wrap.find('.faq-content .row').children(':visible').length == 0) {
                wrap.css('display', 'none');
            }
        });
    }
}