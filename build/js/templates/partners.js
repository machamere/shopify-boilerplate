$(function() {
    logoSize();
    $('[data-to-team]').on('click', function() {
        console.log($(this).data('to-team'))
        var link = $(this).data('to-team');
        $('html, body').animate({
            scrollTop: $("#"+link).offset().top - 85
        }, 800);
    });
    $(window).on('resize', function() {
        logoSize();
    });
});

function logoSize() {
    if($( window ).width() < 992) {
        let maxLogoHeight = $('.team-hero--background').outerHeight()-50;
        $('.team-hero-content--logo img').css("max-height", maxLogoHeight+'px')
    } else {
        $('.team-hero-content--logo img').css("max-height", 'none')
    }
}