/*
 * ToDo: get sync between desktop and mobile for product price on quantity change
 */
$(function() {
    if($('body').hasClass('template-cart')) {
        /*
        * Sticky titles on mobile
        */
        $(window).scroll(sticktothetop);
        sticktothetop();
        $('.cart_remove').on("click", function() {
            $(this).closest('.cart_product').addClass('removed');
        });
        $(".dec, .inc").on("click", function(e) {

            var $button = $(this);
            var oldValue = $button.parent().find("input").val();

            if ($button.hasClass('inc')) {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            $('#update_cart').show();
            $('#go_to_checkout').hide();
            $button.parent().find("input").val(newVal);
        });
        $(".quantity_input").on("change paste keyup", function() {
            $('#update_cart').show();
            $('#go_to_checkout').hide();
        });
        $(".quantity_input").on("click", function() {
            $('#update_cart').show();
            $('#go_to_checkout').hide();
        });
    }
});

function item_total(item) {
    let mobile_adjustment = '';
    let quantity = $(item).find(mobile_adjustment + '.quantity_input').val();
    let id = $(item).data('id');

    $.ajax({
        method: 'POST',
        url: '/cart/change.js',
        dataType: 'json',
        data: {
            quantity: quantity,
            id: id
        }
    });

    cart_total();
}

function cart_total() {
    let total = 0;
    let items = 0;
    let mobile_adjustment = '';
    let total_discount = 0;
    $('.cart_product').each(function() {
        let product = parseFloat($(this).find(mobile_adjustment + '.cart-prod-price').text().trim().replace('$', ''));
        let discount = product;
        if($(this).find(mobile_adjustment + '.discount-holder-value').is(':visible')) {
            discount = parseFloat($(this).find(mobile_adjustment + '.discount-holder-value').text());
        }
        items = parseFloat($(this).find(mobile_adjustment + '.quantity_input').val());
        console.log(items);
        total = (product * items) + total;
        total_discount = (discount * items) + total_discount;
    });
    if(total_discount !== total && $.isNumeric(total_discount)) {
        $("#org_price").show().html("<s>$"+total_discount.toFixed(2) + "</s>");
    }
    $("#subtotal").html("$"+total.toFixed(2));

    $('.cart_count ').text(items);
}

function sticktothetop() {
    let window_top = $(window).scrollTop();
    let top = $('.cart-items-holder').offset().top - 110;
    let footer_top = $(".cart_checkout").offset().top - 110;
    let holder_height = $('.cart_labels.mobile').outerHeight();
    if (window_top + holder_height > footer_top) {
        $('.cart_labels.mobile, .header').removeClass('stick');
        $('.mobile-stick').height(0);
    } else if (window_top > top) {
        $('.mobile-stick').height($('.cart_labels.mobile').outerHeight());
        $('.cart_labels.mobile, .header').addClass('stick');
    } else {
        $('.cart_labels.mobile, .header').removeClass('stick');
        $('.mobile-stick').height(0);
    }
}