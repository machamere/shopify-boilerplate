/**
 * Customer Addresses Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Customer Addresses
 * template.
 *
 * @namespace customerAddresses
 */

import '../vendor/shopify/theme-addresses.js';

const addressSelectors = {
  addressContainer: '[data-address]',
  addressFields: '[data-address-fields]',
  addressToggle: '[data-address-toggle]',
  addressForm: '[data-address-form]',
  addressDeleteForm: '[data-address-delete-form]',
  closeAddressForm: '.modal-close'
};
const hideClass = 'hide';

function initializeAddressForm(container) {
  const addressFields = container.querySelector(addressSelectors.addressFields);
  const addressForm = container.querySelector(addressSelectors.addressForm);
  const deleteForm = container.querySelector(addressSelectors.addressDeleteForm);

  container.querySelectorAll(addressSelectors.addressToggle).forEach((button) => {
    button.addEventListener('click', () => {
      addressForm.classList.toggle(hideClass);
    });
  });

  AddressForm(addressFields, 'en');

  if (deleteForm) {
    deleteForm.addEventListener('submit', (event) => {
      const confirmMessage = deleteForm.getAttribute('data-confirm-message');

      if (!window.confirm(confirmMessage || 'Are you sure you wish to delete this address?')) {
        event.preventDefault();
      }
    });
  }
}

function closeAddressForm(item) {
  var target = item.data('target');
  $('[data-'+target+']').addClass(hideClass);
}

function expandAddress(item) {
  if(item.hasClass('active')) {
    item.removeClass('active').closest('.address-inner').removeClass('active').find('.expanded-menu').slideUp(300);
  } else {
    $('.expand-address').each(function() {
      $(this).removeClass('active').closest('.address-inner').removeClass('active').find('.expanded-menu').slideUp(300);
    });
    item.addClass('active').closest('.address-inner').addClass('active').find('.expanded-menu').slideDown(300);
  }
}

const addressForms = document.querySelectorAll(addressSelectors.addressContainer);
const addressExpand = $('.expand-address');

if (addressForms.length) {
  addressForms.forEach((addressContainer) => {
    initializeAddressForm(addressContainer);
  });

  addressExpand.on('click', function () {
    expandAddress($(this));
  });

  $(addressSelectors.closeAddressForm).on('click', function() {
      closeAddressForm($(this));
  });
}
